= Qaclana Build Image

This is the image that is used in GitLab for building Qaclana. It's based on the `golang` image,
with `protoc` for compilation of the gRPC IDLs and `cockroach` for tests.

== How to build this image

Refer to the `.gitlab-ci.yml`, but basically:
[source,bash]
----
wget https://binaries.cockroachdb.com/cockroach-v1.0.5.linux-amd64.tgz
tar xzf cockroach-v1.0.5.linux-amd64.tgz
rm -f cockroach-v1.0.5.linux-amd64.tgz
mv cockroach-v1.0.5.linux-amd64 cockroach
mkdir protoc
cd protoc
wget https://github.com/google/protobuf/releases/download/v3.3.0/protoc-3.3.0-linux-x86_64.zip
unzip protoc-3.3.0-linux-x86_64.zip
rm -f protoc-3.3.0-linux-x86_64.zip
cd ..
wget https://codeclimate.com/downloads/test-reporter/test-reporter-latest-linux-amd64
chmod +x test-reporter-latest-linux-amd64
docker build -t ${USER}/qaclana-build-image .
----